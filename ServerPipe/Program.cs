﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;

namespace ServerPipe
{
    class Program
    {
        static void Main(string[] args)
        {
            var pipe = new NamedPipeServerStream("MyTest.Pipe", PipeDirection.InOut, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous | PipeOptions.WriteThrough);
            Console.WriteLine("Waiting for connection....");
            pipe.WaitForConnection();
            Console.WriteLine("Connected!");

            new Thread(() =>
            {
                using (StreamReader sr = new StreamReader(pipe))
                {
                    string temp;
                    while ((temp = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(temp);
                    }
                }
            }).Start();

            try
            {
                using (StreamWriter writer = new StreamWriter(pipe))
                {
                    writer.AutoFlush = true;
                    while (true)
                    {
                        var content = Console.ReadLine();
                        writer.WriteLine(content);
                    }
                }
            }
            catch (ObjectDisposedException)
            {
                return;
            }
        }
    }
}
