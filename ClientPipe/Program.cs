﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Threading;

namespace ClientPipe
{
    class Program
    {
        static Process cmd = new Process()
        {
            StartInfo = new ProcessStartInfo()
            {
                FileName = "cmd.exe",
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                RedirectStandardInput = true 
            }
        };

        static ManualResetEventSlim exitEvent = new ManualResetEventSlim(false);

        static void Main(string[] args)
        {
            Console.WriteLine("Client");

            var pipe = new NamedPipeClientStream(".", "MyTest.Pipe", PipeDirection.InOut, PipeOptions.Asynchronous);
            Console.WriteLine("Connecting...");
            pipe.Connect();
            Console.WriteLine("Connected...");
            cmd.Start();
            
            // cmd output thread
            new Thread(() =>
            {
                try
                {
                    using (var writer = new StreamWriter(pipe))
                    {
                        writer.AutoFlush = true;
                        while (!cmd.HasExited)
                        {
                            var cmdContent = cmd.StandardOutput.ReadLine();
                            writer.WriteLine(cmdContent);
                            pipe.WaitForPipeDrain();
                        }
                        writer.Close();
                        writer.Dispose();
                    }
                }
                catch ( IOException)
                {
                    Console.WriteLine("Pipe broken.");
                }
                catch (ObjectDisposedException)
                {
                    Console.WriteLine("Pipe broken 2.");
                }
                cmd.Close();
            })
            { Name = "Cmd output to pipe input" }
            .Start();

            /////////////////////////////////////////////////////////////////////////////
            
            new Thread(() =>
            {
                using (var reader = new StreamReader(pipe))
                {
                    string receivedContent;
                    while ((receivedContent = reader.ReadLine()) != null)
                    {
                        cmd.StandardInput.WriteLine(receivedContent);
                    }
                }
            })
            { Name = "Pipe output to cmd input" }
            .Start();

            /////////////////////////////////////////////////////////////////////////////

            cmd.Exited += (o, e) => { exitEvent.Set(); };

            exitEvent.Wait();
            pipe.Close();
            pipe.Dispose();
        }
    }
}
